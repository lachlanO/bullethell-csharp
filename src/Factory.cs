﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame.src
{
    public class Factory
    {
        private static Factory _instance = new Factory();

        public static Factory Instance
        {
            get
            {
                return _instance;
                    
            }
        }

        static Dictionary<string, Type> _registeredGameObjects = new Dictionary<string, Type>();

        public void Add<T>(string id)
        {
            var type = typeof(T);

            if (type.IsInterface || contains<T>(id))
            {
                throw new Exception();
            }

            _registeredGameObjects.Add(id, type);
        }

        private bool contains<T>(string id)
        {
            var _type = typeof(T);

            if (!_registeredGameObjects.TryGetValue(id, out _type))
            {
                return false;
            }

            else return true;
        }

        public T Create<T>(string id, string myType, params object[] parameters)
        {
            var _type = typeof(T);
            object value = default(T);

            if (!_registeredGameObjects.TryGetValue(id, out _type))
            {
                throw new Exception();
            }

            //Checking to see the type of object being created
            if (_type == typeof(Bullet))
            {
                value = Bullet.CreateBullet(myType, parameters);

            }
            else if (_type == typeof(Wall))
            {
                value = Wall.CreateWall(myType, parameters);
            }
            else if (_type == typeof(Food))
            {
                value = Food.CreateFood(myType, parameters);
            }
            else if (_type == typeof(HealthPack))
            {
                value = HealthPack.CreateHealthPack(myType, parameters);
            }
            else if (_type == typeof(Button))
            {
                value = Button.CreateButton(myType, parameters);
            }
            else value = (T)Activator.CreateInstance(_type, parameters);

            return (T)value;
        }

        public T Create<T>(string id, params object[] parameters)
        {
            var _type = typeof(T);

            if (!_registeredGameObjects.TryGetValue(id, out _type))
            {
                throw new Exception();
            }

            return (T)Activator.CreateInstance(_type, parameters);
        }
    }
}
