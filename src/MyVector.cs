﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    public class MyVector
    {
        private float _x;
        private float _y;


        public MyVector(int X, int Y)
        {
            _x = (float)X;
            _y = (float)Y;
        }

        public MyVector(double X, double Y)
        {
            _x = (float)X;
            _y = (float)Y;

        }

        public MyVector(float X, float Y)
        {
            _x = X;
            _y = Y;
        }

        public MyVector()
        {
            _x = 0;
            _y = 0;
        }

        public float X
        {
            get
            {
                return _x;
            }

            set
            {
                _x = (float)value;
            }
        }

        public float Y
        {
            get
            {
                return _y;
            }

            set
            {
                _y = (float)value;
            }
        }

        public float Length
        {
            get 
            {
                return (float)Math.Sqrt((Math.Abs(_x * _x) + Math.Abs(_y * _y)));
            }
        }

        public MyVector UnitVector
        {
            get
            {
                float unitX = _x/Length;
                float unitY = _y/Length;
                return new MyVector(unitX, unitY);
            }
        }

        public void Limit(float lim)
        {
            if (_x > lim || _y >lim)
            {
                _x = (_x / Length) * lim;
                _y = (_y / Length) * lim;
            }
        }

        public void sub(MyVector v)
        {
            _x -= v.X;
            _y -= v.Y;
        }

        public Point2D ConvertToPoint2D()
        {
            Point2D a = new Point2D();
            a.X = _x;
            a.Y = Y;
            return a;
        }

        public static MyVector operator* (MyVector b, float num)
        {
            MyVector a = new MyVector(b.X * num, b.Y * num);
            return a;

        }

        public static MyVector operator* (float num, MyVector b)
        {
            MyVector a = new MyVector(b.X * num, b.Y * num);
            return a;

        }

        public static MyVector operator/ (MyVector b, float num)
        {
            MyVector a = new MyVector(b.X / num, b.Y / num);
            return a;

        }

        public static MyVector operator+ (MyVector b, MyVector c)
        {
            MyVector a = new MyVector(b.X + c.X, b.Y + c.Y);
            return a;

        }
        
        public static MyVector operator- (MyVector b, MyVector c)
        {
            MyVector a = new MyVector(b.X - c.X, b.Y - c.Y);
            return a;
        }
    }
}
