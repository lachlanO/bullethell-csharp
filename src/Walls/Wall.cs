﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    public abstract class Wall: IRenderable
    {
        //Ensures that wall doesn't spawn with 10 pixels of the players spawn point
        protected const int BUFFER_DISTANCE = 10;

        protected Player _player;

        //ensures the bullet doesn't spawn on the player
        protected bool inPlayer;

        protected MyVector _position;
        protected float _width;
        protected float _height;
        protected Color _clr;

        protected static Random rng = new Random();

        protected int iteration;

        protected Rectangle me;


        private static Dictionary<string, Type> _wallClassRegistry = new Dictionary<string, Type>();

        public static void RegisterWall(string name, Type t)
        {
            _wallClassRegistry[name] = t;
        }

        public static Wall CreateWall(string name, params object[] parameters)
        {
            return (Wall)Activator.CreateInstance(_wallClassRegistry[name], parameters);
        }


        public MyVector Position
        {
            get
            {
                return _position;
            }
        }

        public float Width
        {
            get
            {
                return _width;
            }
        }

        public float Height
        {
            get
            {
                return _height;
            }
        }

        public Rectangle Me
        {
            get
            {
                return me;
            }
        }

        public Wall(Player p)
        {
            _player = p;
            _clr = Color.White;

            _position = new MyVector();

            inPlayer = true;
            Respawn();
            me = new Rectangle();
        }

        protected bool checkPlayerPosition()
        {
            return SwinGame.PointInRect(_player.Position.X + (_player.Width / 2), _player.Position.Y + (_player.Height / 2), _position.X, _position.Y, _height, _width);
        }

        protected virtual void Respawn()
        {
            int screenHeight = SwinGame.ScreenWidth();
            int screenWidth = SwinGame.ScreenWidth();

            while (inPlayer)
            {
                switch (rng.Next(1, 5))
                {
                    case 1:
                        //Ensures that spawn is 10 pixels away from players spawn
                        _position.X = rng.Next(0, screenWidth / 2 - BUFFER_DISTANCE);
                        _position.Y = rng.Next(0, screenHeight - (int)_height);
                        break;

                    case 2:
                        //Ensures that spawn is 10 pixels away from players spawn
                        _position.X = rng.Next(screenWidth / 2 + (int)(_player.Width + BUFFER_DISTANCE), screenWidth - (int)_width);
                        _position.Y = rng.Next(0, screenHeight - (int)_height);
                        break;

                    case 3:
                        //Ensures that spawn is 10 pixels away from players spawn
                        _position.X = rng.Next(0, screenWidth - (int)_width);
                        _position.Y = rng.Next(0, screenHeight / 2 - BUFFER_DISTANCE);
                        break;

                    case 4:
                        //Ensures that spawn is 10 pixels away from players spawn
                        _position.X = rng.Next(0, screenWidth - (int)_width);
                        _position.Y = rng.Next(screenHeight / 2 + (int)(_player.Height + BUFFER_DISTANCE), screenHeight - (int)_height);
                        break;
                }

                inPlayer = checkPlayerPosition();
            }

            iteration = 0;
        }

        public virtual void Update()
        {

            me.X = _position.X;
            me.Y = _position.Y;
            me.Width = _width;
            me.Height = _height;
        }

        public abstract void Draw();
    }
}
