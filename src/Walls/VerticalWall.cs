﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    class VerticalWall: Wall
    {
        public VerticalWall(Player p) :base(p)
        {

        }

        public override void Update()
        {
            iteration++;

            if (iteration >= 300)
            {
                inPlayer = true;
                Respawn();
            }

            base.Update();
        }

        public override void Draw()
        {
            SwinGame.FillRectangle(_clr, _position.X, _position.Y, _width, _height);
        }

        protected override void Respawn()
        {
            _width = rng.Next(30, 36);
            _height = rng.Next(80, 120);

            base.Respawn();
        }
    }
}
