﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    class HorizontalWall: Wall
    {
        public HorizontalWall(Player p) :base(p)
        {

        }

        public override void Update()
        {
            iteration++;

            if (iteration >= 300)
            {
                inPlayer = true;
                Respawn();
            }
            base.Update();
        }

        public override void Draw()
        {
            SwinGame.FillRectangle(_clr, _position.X, _position.Y, _width, _height);
        }

        protected override void Respawn()
        {
            _width = rng.Next(90, 130);
            _height = rng.Next(20, 35);

            base.Respawn();           
        }
    }
}
