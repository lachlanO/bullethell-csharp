﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    class SpeedChangeBullet: Bullet
    {
        //The Two different speeds of the bullet
        private const float MAX_SPEED = 4;
        private const float MIN_SPEED = 2;

        //bool to determine which speed the bullet is travelling
        private bool movingFast = false;

        //An Integer that will be compared to a random number in order to determine when a speed change occurs
        private int chanceOfSpeedChange;

        private static Random rng = new Random();
        

        public SpeedChangeBullet(Player p, float width) :base(p)
        {
            _speed = MIN_SPEED;
            _clr = Color.Orange;
            _width = width;

            SetValues();
        }

        public SpeedChangeBullet(Player p) :this(p, 5)
        {

        }

        public override void Update()
        {
            _position += _velocity;

            if (checkReposition())
            {
                SetValues();
            }

            if (checkSpeedChange())
            {
                chanceOfSpeedChange = 1;

                if (movingFast)
                {
                    movingFast = false;
                    _speed = MIN_SPEED;
                }
                else
                {
                    movingFast = true;
                    _speed = MAX_SPEED;
                }

                setSpeed();
            }
        }

        public override void SetValues()
        {
            _dist.X = _player.Position.X + 10 - _position.X;
            _dist.Y = _player.Position.Y + 20 - _position.Y;

            setSpeed();

            chanceOfSpeedChange = 1;

        }

        private void setSpeed()
        {
            _velocity = _speed * _dist.UnitVector;
        }

        private bool checkSpeedChange()
        {
            if (chanceOfSpeedChange >= rng.Next(1, 501))
            {
                return true;
            }
            else
            {
                chanceOfSpeedChange++;
                return false;
            }
        }

        public override bool CheckHit()
        {
            if(IsAt())
            {
                return true;
            }
            return false;
        }

        public override void Draw()
        {
            SwinGame.DrawRectangle(_clr, _position.X, _position.Y, _width, _width);
        }
        
    }
}
