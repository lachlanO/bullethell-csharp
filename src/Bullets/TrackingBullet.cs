﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    class TrackingBullet: Bullet
    {
        private MyVector _direction;
        private MyVector _steering;
        private MyVector _maxVelocity;
        private MyVector _acceleration;

        //bullets turning ability
        private float _maxForce;


        public TrackingBullet(Player p, int speed, float width, float maxForce) :base(p)
        {
            _speed = speed;
            _width = width;
            _maxForce = maxForce;
            _clr = Color.Red;

            _maxVelocity = new MyVector();
            _steering = new MyVector();
            _acceleration = new MyVector();

            SetValues();
        }

        public TrackingBullet(Player p) :this(p, 3, 5, .09f)
        {

        }

        public override void Update()
        {
            _velocity +=_acceleration;
            _velocity.Limit(_speed);
            _position += _velocity;
            _acceleration *= 0;

            SetValues();
            

            if (checkReposition())
            {
                SetValues();
            }

        }

        public override void SetValues()
        {
            _dist.X = (_player.Position.X + 10 - _position.X);
            _dist.Y = (_player.Position.Y + 20 - _position.Y);

            _direction = _dist.UnitVector;

            _maxVelocity = _direction * _speed;

            _steering = _maxVelocity - _velocity;
            _steering.Limit(_maxForce);

            //applying steering force
            _acceleration += _steering;
        }

        public override void Draw()
        {
            SwinGame.DrawRectangle(_clr, _position.X, _position.Y, _width, _width);
        }

        public override bool CheckHit()
        {
            if (IsAt())
            {
                return true;
            }
            return false;
        }

        
    }
}
