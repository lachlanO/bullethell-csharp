﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    public abstract class Bullet: IRenderable
    {
        private static Random rnd = new Random();

        //speed bullets move increases as game progresses
        protected float _speed;

        protected MyVector _dist;
        protected MyVector _position;
        protected MyVector _velocity;
        protected Color _clr;
        protected float _width;
        protected Player _player;


        private static Dictionary<string, Type> _bulletClassRegistry = new Dictionary<string, Type>();

        public static void RegisterBullet(string name, Type t)
        {
            _bulletClassRegistry[name] = t;
        }

        public static Bullet CreateBullet(string name, params object[] parameters)
        {
            return (Bullet)Activator.CreateInstance(_bulletClassRegistry[name], parameters);
        }

        public Bullet(Color c)
        {
            _clr = c;
        }

        public Bullet(Player p):this(Color.Yellow)
        {

            _position = new MyVector();
            _velocity = new MyVector();
            _dist = new MyVector();
            _player = p;
            Respawn();
        }

        
        public void Respawn()
        {
            //Selecting whether bullet will spawn above, bellow, right or left of GUI
            int height = SwinGame.ScreenWidth();
            int width = SwinGame.ScreenWidth();
            switch (rnd.Next(1, 5))
            {
                
                //top
                case 1:
                    _position.X = rnd.Next(0, width);
                    _position.Y = rnd.Next(-100, 0);
                    break;
        
                //bottom
                case 2:
                    _position.X = rnd.Next(0, width);
                    _position.Y = rnd.Next(height, height+100);
                    break;
        
                //right
                case 3:
                    _position.X = rnd.Next(width, width+100);
                    _position.Y = rnd.Next(0, height);
                    break;
        
                //left
                case 4:
                    _position.X = rnd.Next(-100, 0);
                    _position.Y = rnd.Next(0, height);
                    break;
            }
        }

        protected bool checkReposition()
        {
            if (_position.X > 1000 || _position.X < -200 || _position.Y < -200 || _position.Y > 800)
            {
                Respawn();
                return true;
            }
            return false;
        }

        public abstract void SetValues();

        public abstract void Draw();

        public abstract void Update();
        
        public abstract bool CheckHit();

        protected bool IsAt()
        {
            return SwinGame.PointInRect(_position.X, _position.Y, _player.Me);
        }
    }
}
