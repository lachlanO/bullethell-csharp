﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    class CurveBullet: Bullet
    {

        private MyVector _center;
        private float _angleInRads;
        private float _radius;
        private int _iteration;

        //number of ticks prior to bullet exiting loops (60 ticks a second)
        private int _ticks;


        public CurveBullet(Player p, int speed, float width) : base(p)
        {
            _speed = speed;
            _clr = Color.Yellow;
            _width = width;  
            _center = new MyVector();

            SetValues();
        }            
        
        public CurveBullet(Player p) :this(p, 3, 5)
        {

        }

        public override void Update()
        {

            if (_ticks == _iteration)
            {
                _position += _velocity;
                
            }
            else
            {
                
                _position.X = _radius * (float)Math.Cos(_angleInRads) + _center.X;
                _position.Y = _radius * (float)Math.Sin(_angleInRads) + _center.Y;
                _angleInRads += (float)Math.PI / _ticks;
                _iteration++;
            }


            if(checkReposition())
            {
                SetValues();
            }
                
        }

        private void initAngle()
        {
            if (_position.X < _player.Position.X)
            {
                if (_position.Y > _player.Position.Y)
                {
                    _angleInRads += (float)Math.PI;
                }
                else
                {
                    _angleInRads -= (float)Math.PI;
                }
            }
        }

        public override void Draw()
        {
            SwinGame.DrawRectangle(_clr, _position.X, _position.Y, _width, _width);

        }

        public override void SetValues()
        {
            _dist = _player.Position - _position;

            _radius = .5f * _dist.Length;
            _angleInRads = (float)Math.Atan(_dist.Y / _dist.X);
            initAngle();

            _velocity.X = 3 * (float)Math.Cos(_angleInRads - Math.PI / 2);
            _velocity.Y = 3 * (float)Math.Sin(_angleInRads - Math.PI / 2);

            _center = (_player.Position + _position) / 2;

            _iteration = 0;
            _ticks = 180;
        }

        public override bool CheckHit()
        {
            if (IsAt())
            {
                return true;
            }
            return false;
        }
    }
}
