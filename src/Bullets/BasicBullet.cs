﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    class BasicBullet: Bullet
    {

        public BasicBullet(Player p, int speed, float width) : base(p)
        {
            _speed = speed;
            _clr = Color.Aqua;
            _width =width;

            SetValues();           
        }

        public BasicBullet(Player p) :this(p, 3, 5)
        {

        }

        public override void Update()
        {
            _position += _velocity;

            if (checkReposition())
            {
                SetValues();
            }
        }

        public override void Draw()
        {
            SwinGame.DrawRectangle(_clr, _position.X, _position.Y,_width, _width);
            
        }

        public override void SetValues()
        {
            _dist.X = _player.Position.X + 10 - _position.X;
            _dist.Y = _player.Position.Y + 20 - _position.Y;

            _velocity = _speed * _dist.UnitVector;
        }

        public override bool CheckHit ()
        {
            if (IsAt())
            {
                return true;
            }
            return false;
        }
    }
}
