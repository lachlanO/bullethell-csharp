using System;
using SwinGameSDK;

namespace MyGame.src
{
    public class GameMain
    {
        public static void Main()
        {
            //Open the game window
            SwinGame.OpenGraphicsWindow("BULLETS!!!", 800, 600);
           

            //inserting bullet types into dictionary
            Bullet.RegisterBullet("BasicBullet", typeof(BasicBullet));
            Bullet.RegisterBullet("CurveBullet", typeof(CurveBullet));
            Bullet.RegisterBullet("TrackingBullet", typeof(TrackingBullet));
            Bullet.RegisterBullet("SpeedChangeBullet", typeof(SpeedChangeBullet));

            //inserting wall types into dictionary
            Wall.RegisterWall("HorizontalWall", typeof(HorizontalWall));
            Wall.RegisterWall("VerticalWall", typeof(VerticalWall));

            //inserting food types into dictionary
            Food.RegisterFood("Pizza", typeof(Pizza));

            //inserting health pack types into dictionary
            HealthPack.RegisterHealthPack("LargeHealthPack", typeof(LargeHealthPack));
            HealthPack.RegisterHealthPack("SmallHealthPack", typeof(SmallHealthPack));

            //inserting button types into dictionary
            Button.RegisterButton("ExitButton", typeof(ExitButton));
            Button.RegisterButton("RestartButton", typeof(RestartButton));


            Game myGame = Game.GetInstance;


            //Run the game loop
            while (false == SwinGame.WindowCloseRequested())
            {
                //Fetch the next batch of UI interaction
                SwinGame.ProcessEvents();

                
                
                //Clear the screen and draw the framerate
                SwinGame.ClearScreen(Color.Black);
                SwinGame.DrawFramerate(0,0);

                myGame.Update();

                //Draw onto the screen
                SwinGame.RefreshScreen(60);
            }
        }
    }
}