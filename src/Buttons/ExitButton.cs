﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    public class ExitButton: Button
    {
        public ExitButton(float X, float Y, float width, float height, string txt) :base(X,Y,width,height, txt)
        {

        }
        public ExitButton() :this(20, SwinGame.ScreenHeight() - 70, 100, 60, "Exit")
        {

        }

        public override void Execute()
        {
            Environment.Exit(0);
        }
    }
}
