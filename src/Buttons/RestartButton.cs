﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    public class RestartButton: Button
    {
        private Game _myGame = Game.GetInstance;


        public RestartButton(float X, float Y, float width, float height, string txt) :base(X,Y,width,height, txt)
        {

        }

        public RestartButton() :this(SwinGame.ScreenWidth() - 120, SwinGame.ScreenHeight() - 70, 100, 60, "Restart")
        {

        }

        public override void Execute()
        {
            _myGame.Restart();
        }
    }
}
