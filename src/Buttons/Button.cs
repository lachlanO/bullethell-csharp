﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    public abstract class Button: IRenderable
    {
        
        protected Rectangle _btn;

        protected Color _clr;
        protected Color _txtClr;
        protected string _txt;



        private static Dictionary<string, Type> _buttonClassRegistry = new Dictionary<string, Type>();

        public static void RegisterButton(string name, Type t)
        {
            _buttonClassRegistry[name] = t;
        }

        public static Button CreateButton(string name, params object[] parameters)
        {
            return (Button)Activator.CreateInstance(_buttonClassRegistry[name], parameters);
        }





        public Button(float X, float Y, float Width, float Height, string txt) :this(Color.LightGreen, Color.Black)
        {
            _btn = new Rectangle();

            _btn.X = X;
            _btn.Y = Y;
            _btn.Width = Width;
            _btn.Height = Height;

            _txt = txt;

            
        }

        public Button(Color C, Color textC)
        {
            _clr = C;
            _txtClr = textC;
        }

        public void Draw()
        {
            SwinGame.FillRectangle(_clr, _btn);
            SwinGame.DrawText(_txt, _txtClr, _btn.X + 30, _btn.Y + 25);
        }

        public void Update()
        {
            if (checkClick()) Execute();
        }

        private bool checkClick()
        {
            if (SwinGame.MouseClicked(MouseButton.LeftButton) && SwinGame.PointInRect(SwinGame.MousePosition(), _btn))
            {
                return true;
            }
            else return false;
        }

        public  abstract void Execute();
    }
}
