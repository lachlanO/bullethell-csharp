﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    class SmallHealthPack: HealthPack
    {
        private Circle me;
        private float _radius;

        //number of iterations prior to healthpack despawning
        private int _lifeLength;


        public SmallHealthPack(Player p, float radius, int lifeLength) :base(p, 600, 1)
        {
            me = new Circle();
            _radius = radius;
            _lifeLength = lifeLength;
        }

        public SmallHealthPack(Player p) :this(p, 5, 300)
        {

        }

        public override void Update()
        {
            base.Update();

            if (iteration >= _lifeLength)
            {
                isSpawned = false;
                _position.X = -20;
                updateMe();
                iteration = 0;
            }
        }

        protected override void updateMe()
        {
            me.Center = _position.ConvertToPoint2D();
            me.Radius = _radius;
        }

        public override void Draw()
        {
            if(isSpawned)SwinGame.FillCircle(_clr, me);
        }
    }
}
