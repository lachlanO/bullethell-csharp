﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    public class Pizza : Food
    {
        private int _lengthLife; 

        public Pizza(Player p, int lifeLength) :base(p)
        {
            _clr = Color.OrangeRed;
            _lengthLife = lifeLength;
        }

        public Pizza(Player p) :this(p, 300)
        {

        }

        public override void Heal()
        {
            if (_player.Health < 5 && _player.Fullness < 100)
            {
                _player.Health++;  
            }
            _player.Fullness += 20;
        }

        public override void Update()
        {
            base.Update();

            if (checkHit())
            {
                Heal();
                respawn();
            }

            if (iteration >= _lengthLife)
            {
                respawn();
                iteration = 0;
            }

        }

        public override void Draw()
        {
            SwinGame.DrawTriangle(_clr, _position.X, _position.Y, _position.X - 4, _position.Y - 10, _position.X + 4, _position.Y - 10);
        }

        
        
    }
}
