﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    public abstract class Food: IHeal, IRenderable
    {
        private static Random rnd = new Random();
        protected MyVector _position;
        protected Player _player;
        protected Color _clr;

        //used to respawn food
        protected int iteration = 0;



        private static Dictionary<string, Type> _foodClassRegistry = new Dictionary<string, Type>();

        public static void RegisterFood(string name, Type t)
        {
            _foodClassRegistry[name] = t;
        }

        public static Food CreateFood(string name, params object[] parameters)
        {
            return (Food)Activator.CreateInstance(_foodClassRegistry[name], parameters);
        }



        public Food(Player p) :this(Color.OrangeRed)
        {
            _player = p;
        }

        public Food(Color clr)
        {
            _position = new MyVector();
            _clr = clr;
            respawn();
        }

        public abstract void Heal();

        public abstract void Draw();

        public virtual void Update()
        {
            iteration++;
        }

        protected bool checkHit()
        {
            return SwinGame.PointInRect(_position.X, _position.Y, _player.Me);
        }

        protected void respawn()
        {
            _position.X = rnd.Next(0, SwinGame.ScreenWidth());
            _position.Y = rnd.Next(0, SwinGame.ScreenHeight());
        }
    }
}
