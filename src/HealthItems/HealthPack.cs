﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    public abstract class HealthPack: IHeal, IRenderable
    {
        protected static Random rng = new Random();
        protected MyVector _position;
        protected Player _player;
        protected Color _clr;

        private static Dictionary<string, Type> _healthPackClassRegistry = new Dictionary<string, Type>();

        public static void RegisterHealthPack(string name, Type t)
        {
            _healthPackClassRegistry[name] = t;
        }

        public static HealthPack CreateHealthPack(string name, params object[] parameters)
        {
            return (HealthPack)Activator.CreateInstance(_healthPackClassRegistry[name], parameters);
        }

        //varies on rarity of health pack
        protected int _chanceOfSpawn;

        protected int _healAmmount;

        //health packs spawn at random intervals, not necessarily immediateley
        protected bool isSpawned;

        protected int iteration;

        public HealthPack(Player p, int chanceSpawn, int healAmmount) :this(Color.Green)
        { 
            _position = new MyVector();
            _chanceOfSpawn = chanceSpawn;

            iteration = 0;

            _player = p;
            _healAmmount = healAmmount;
            Respawn();
        }
        
        public HealthPack(Color clr)
        {
            _clr = clr;
        }

        public void Heal()
        {
            _player.Health += _healAmmount;
        }

        public abstract void Draw();

        public virtual void Update()
        {
            iteration++;

            checkRespawn();

            if (checkHit())
            {
                isSpawned = false;
                //ensures player can't pick up pack multiple times
                _position.X = -20;
                updateMe();
                iteration = 0;

                Heal();
            }
        }

        protected bool checkHit()
        {
            return SwinGame.PointInRect(_position.X, _position.Y, _player.Me);
        }

        protected void Respawn()
        {
            _position.X = rng.Next(0, SwinGame.ScreenWidth());
            _position.Y = rng.Next(0, SwinGame.ScreenHeight());
            updateMe();

        }

        protected abstract void updateMe();

        private void checkRespawn()
        {
            if (!isSpawned)
            {
                
                if (iteration > rng.Next(1, _chanceOfSpawn))
                {
                    Respawn();
                    isSpawned = true;
                    iteration = 0;
                }
            }
        }

    }
}
