﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    class LargeHealthPack: HealthPack
    {
        private float _width;
        private Rectangle me;

        //number used to determine the health packs chance of despawning
        private int _chanceOfDespawn;

        public LargeHealthPack(Player p, float width, int chanceDespawn) :base(p, 5000, 3)
        {
            me = new Rectangle();
            _width = width;
            _chanceOfDespawn = chanceDespawn;
        }

        public LargeHealthPack(Player p) :this(p, 15, 5000)
        {

        }

        public override void Update()
        {
            base.Update();

            if (iteration >= rng.Next(1, _chanceOfDespawn))
            {
                isSpawned = false;
                _position.X = -20;
                updateMe();
                iteration = 0;
            }
        }

        protected override void updateMe()
        {
            me.X = _position.X;
            me.Y = _position.Y;
            me.Width = _width;
            me.Height = _width;
        }

        public override void Draw()
        {
            if(isSpawned) SwinGame.FillRectangle(_clr, me);
        }

    }
}
