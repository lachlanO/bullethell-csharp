﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;

namespace MyGame.src
{
    public class Game
    {
        private static Game instance = null;
       //private LeaderBoard _leaderboard = new LeaderBoard();
        private int _score = 0;
        private Player p;

        //used to increase difficulty
        private int iteration;
        private static Random rng = new Random();

        // makes bullets respawn when player hit
        private bool _bulletRespawn;

        //used for displayer information
        private string _scoreString = "";
        private string _livesString = "";
        private string _fullnessString = "";

        private List<IRenderable> _gameItems = new List<IRenderable>();
        private List<Bullet> _bullets = new List<Bullet>();
        private List<Wall> _walls = new List<Wall>();

        private Factory _myObjectFactory = Factory.Instance;

        private bool _gameRestarted = false;
        
        public Player P
        {
            get
            {
                return p;
            }
        }

        public List<Bullet> Bullets
        {
            get
            {
                return _bullets;
            }
        }

        public List<IRenderable> GameItemsList
        {
            get
            {
                return _gameItems;
            }
        }
        
        
        private Game()
        {
            _myObjectFactory.Add<Player>("Me");
            _myObjectFactory.Add<Bullet>("Bullet");
            _myObjectFactory.Add<Wall>("Wall");
            _myObjectFactory.Add<Food>("Food");
            _myObjectFactory.Add<HealthPack>("HealthPack");
            _myObjectFactory.Add<Button>("Button");

            createInstancesStart();
            addInstancesIRenderable();

            _bulletRespawn = false;
            iteration = 0;
        }

        public static Game GetInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Game();
                }
                return instance;
            }
        }

        private void createInstancesStart()
        {
            p = _myObjectFactory.Create<Player>("Me");
            _gameItems.Add(p);

            for (int i = 0; i < 3; i++)
            {
                _walls.Add(_myObjectFactory.Create<Wall>("Wall", "HorizontalWall", p));
                _walls.Add(_myObjectFactory.Create<Wall>("Wall", "VerticalWall", p));
                _bullets.Add(_myObjectFactory.Create<Bullet>("Bullet", "BasicBullet", p));
            }

            _bullets.Add(_myObjectFactory.Create<Bullet>("Bullet", "TrackingBullet", p));
            _gameItems.Add(_myObjectFactory.Create<HealthPack>("HealthPack", "LargeHealthPack", p));
            _gameItems.Add(_myObjectFactory.Create<HealthPack>("HealthPack", "SmallHealthPack", p));
            _gameItems.Add(_myObjectFactory.Create<Food>("Food", "Pizza", p));


            for (int i = 0; i <= 2; i++)
            {
                _bullets.Add(_myObjectFactory.Create<Bullet>("Bullet", "SpeedChangeBullet", p));
                _bullets.Add(_myObjectFactory.Create<Bullet>("Bullet", "CurveBullet", p));
            }
        }

        private void addInstancesIRenderable ()
        {
            foreach(Wall w in _walls)
            {
                if (!_gameItems.Contains(w))
                {
                    _gameItems.Add(w);
                }
            }

            foreach (Bullet b in _bullets)
            {
                if (!_gameItems.Contains(b))
                {
                    _gameItems.Add(b);
                }
            }
        }

        public void Update()
        {
            if (p.Health > 0)
            {
                iteration++;

                _scoreString = "Score: " + _score.ToString();
                _livesString = "Lives: " + p.Health;
                _fullnessString = "Fullness: " + p.Fullness + "%";

                //displaying important information
                SwinGame.DrawText(_livesString, Color.LightGreen,(SwinGame.ScreenWidth() - 120), 5);
                SwinGame.DrawText(_scoreString, Color.LightGreen, (SwinGame.ScreenWidth() - 120), 20);
                SwinGame.DrawText(_fullnessString, Color.LightGreen, (SwinGame.ScreenWidth() - 120), 35);

                p.GetWalls(_walls);

                foreach (IRenderable i in _gameItems)
                {
                    i.Update();
                    Console.WriteLine("test");
                    i.Draw();
                }

                foreach (Bullet b in _bullets)
                {
                    if (b.CheckHit())
                    {
                        p.hit();

                        _bulletRespawn = true;
                        gameOver();
                        break;
                    }
                }
                if (_bulletRespawn) respawnBullets();

                if (checkIncreaseDifficulty()) increaseDifficulty();

                _score += 1;
            }
            else
            {
                SwinGame.DrawText("Game over", Color.White, SwinGame.ScreenWidth() / 2 - 20, SwinGame.ScreenHeight() / 2);
                SwinGame.DrawText(_scoreString, Color.White, SwinGame.ScreenWidth() / 2 - 23, SwinGame.ScreenHeight() / 2 + 15);
                for (int i = _gameItems.Count; i > 0; i--)
                {
                    _gameItems[i - 1].Draw();
                    _gameItems[i - 1].Update();
                    if (_gameRestarted) break;
                }
            }
            
        }

        private void respawnBullets()
        {
            _bulletRespawn = false;
            foreach(Bullet b in _bullets)
            {
                b.Respawn();
                b.SetValues();
            }
        }

        private void gameOver()
        {
            if (p.Health <= 0)
            {
                _gameRestarted = false;
                _gameItems.Clear();
                _gameItems.Add(_myObjectFactory.Create<Button>("Button", "ExitButton"));
                _gameItems.Add(_myObjectFactory.Create<Button>("Button", "RestartButton"));
            }
        }

        private void increaseDifficulty()
        {
            switch (rng.Next(1, 4))
            {
                case 1:
                    _bullets.Add(_myObjectFactory.Create<Bullet>("Bullet", "BasicBullet", p));
                    break;

                case 2:
                    _bullets.Add(_myObjectFactory.Create<Bullet>("Bullet", "SpeedChangeBullet", p));
                    break;

                case 3:
                    _bullets.Add(_myObjectFactory.Create<Bullet>("Bullet", "CurveBullet", p));
                    break;
            }
            addInstancesIRenderable();
        }
        
        private bool checkIncreaseDifficulty()
        {
            if (iteration % 300 == 0) return true;

            else return false;
        }

        public void Restart()
        {
            _score = 0;
            _walls.Clear();
            _bullets.Clear();
            _gameItems.Clear();
            p.Reset();
            _gameRestarted = true;

            createInstancesStart();
            addInstancesIRenderable();
        }


    }
}
