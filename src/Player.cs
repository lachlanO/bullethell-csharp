﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SwinGameSDK;

namespace MyGame.src
{
    public class Player: Object, IRenderable
    {
        private MyVector _position;
        private int _health;
        private Color _clr;
        private float _width;
        private float _height;

        private Rectangle me;

        private int iteration;

        //reference to the Wall list
        private List<Wall> _walls;

        //Used to determine when a person is able to eat
        private int _fullness;

        //boolean to check whether a player can move (nothing is blocking its path)
        private bool canMove;

        public MyVector Position
        {
            get
            {
                return _position;
            }
        }

        public float Width
        {
            get
            {
                return _width;
            }
        }

        public float Height
        {
            get
            {
                return _height;
            }
        }

        public int Health
        {
            get
            {
                return _health;
            }
            set
            {
                _health = value;
            }
        }

        public Rectangle Me
        {
            get
            {
                return me;
            }
        }

        public int Fullness
        {
            get
            {
                return _fullness;
            }
            set
            {
                _fullness = value;
            }
        }

        public Player(): this(5, 20, 40, Color.Red)
        {
            
        }

        public Player(int hp, float width, float height, Color c)
        {
            _clr = c;
            _health = hp;
            _width = width;
            _height = height;
            _fullness = 0;
            iteration = 0;

            _position = new MyVector();
            _position.X = 400;
            _position.Y = 300;

            me = new Rectangle();
        }

        public void Draw()
        {
            SwinGame.FillRectangle(_clr, _position.X, _position.Y, _width, _height);
        }

        public void hit()
        {
            _health -= 1;
            _position.X = 400;
            _position.Y = 300;
            
        }

        public void GetWalls(List<Wall> w)
        {
            _walls = w;
        }


        public void Update()
        {
            canMove = true;

            if (SwinGame.KeyDown(KeyCode.RightKey) && _position.X < SwinGame.ScreenWidth() - _width)
            {
                
                foreach(Wall w in _walls)
                {
                    if (SwinGame.PointInRect(_position.X + Width, Position.Y + Height/2, w.Me))
                    {
                        //ensures player doesn't get stuck
                        _position.X = w.Position.X - _width - 1;
                        canMove = false;
                        break;
                    }
                }
                
                if (canMove) _position.X += 5;
            }
                

            if (SwinGame.KeyDown(KeyCode.LeftKey) && _position.X > 0)
            {
                
                foreach(Wall w in _walls)
                {
                    if (SwinGame.PointInRect(_position.X, Position.Y + Height/2, w.Me))
                    {
                        //ensures player doesn't get stuck
                        _position.X = w.Position.X + w.Width + 1;
                        canMove = false;
                        break;
                    }
                }
                
                if (canMove) _position.X -= 5;
            }
                

            if (SwinGame.KeyDown(KeyCode.UpKey) && _position.Y > 0)
            {

                
                foreach(Wall w in _walls)
                {
                    if (SwinGame.PointInRect(_position.X + Width/2, Position.Y, w.Me))
                    {
                        //ensures player doesn't get stuck
                        _position.Y = w.Position.Y + w.Height + 1;
                        canMove = false;
                        break;
                    }
                }
                
                if (canMove) _position.Y -= 5;
            }

            if (SwinGame.KeyDown(KeyCode.DownKey) && _position.Y < SwinGame.ScreenHeight() - _height)
            {
                
                foreach(Wall w in _walls)
                {
                    if (SwinGame.PointInRect(_position.X + _width/2, Position.Y + _height, w.Me))
                    {
                        //ensures player doesn't get stuck
                        _position.Y = w.Position.Y - _height - 1;
                        canMove = false;
                        break;
                    }
                }
                
                if (canMove) _position.Y += 5;
            }

            reduceFullness();
            updateMe();
        }

        private void reduceFullness()
        {
            iteration++;
            if (iteration % 60 == 0 && _fullness > 0)
            {
                _fullness -= 2;
            }
            if (_fullness < 0) _fullness = 0;
        }

        private void updateMe()
        {
            me.X = _position.X;
            me.Y = _position.Y;
            me.Width = _width;
            me.Height = _height;
        }

        public void Reset()
        {
            _position.X = 400;
            _position.Y = 300;
            _health = 5;
        }


    }
}
